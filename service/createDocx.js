var fs = require("fs");
var JSZip = require("jszip");
const { xsltProcess, xmlParse } = require("xslt-processor");
const { fetchXml } = require("./json2xml");

function createDocx() {
  var zip = new JSZip();
  const zipTmpl = fs.readFileSync("./templates/Dossier.zip");
  const xsltString = fs.readFileSync("./service/Books.xslt", "utf8");
  // Fake call to get XML
  const xmlString = fetchXml();
  const outXmlString = xsltProcess(xmlParse(xmlString), xmlParse(xsltString));

  return zip.loadAsync(zipTmpl).then(zip => {
    zip.file("word/document.xml", outXmlString);
    zip
      .generateNodeStream({ streamFiles: true })
      .pipe(fs.createWriteStream("out.docx"))
      .on("finish", function() {
        console.log("out.docx written.");
      });
    return zip.generateAsync({ type: "nodebuffer", streamFiles: true });
  });
}

module.exports.createDocx = createDocx;
