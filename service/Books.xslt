<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<w:document
			xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
			xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
			xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
			xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
			xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
			xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
			xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
			xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
			xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
			xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
			xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
			xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
			xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
			xmlns:o="urn:schemas-microsoft-com:office:office"
			xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
			xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
			xmlns:v="urn:schemas-microsoft-com:vml"
			xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
			xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
			xmlns:w10="urn:schemas-microsoft-com:office:word"
			xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
			xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
			xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
			xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
			xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
			xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
			xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
			xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
			xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape" mc:Ignorable="w14 w15 w16se w16cid wp14">
			<w:body>
				<w:p w14:paraId="088C67F7" w14:textId="7091A84C" w:rsidR="00344C7E" w:rsidRDefault="000B2851" w:rsidP="000B2851">
					<w:pPr>
						<w:pStyle w:val="Heading2" />
					</w:pPr>
					<w:r>
						<w:t>Books</w:t>
					</w:r>
				</w:p>
				<!-- for-each loop added for Genre.  This loop includes the Open XML elements for the paragraph the Genre placeholder is in and all paragraphs for the Movies. -->
				<xsl:for-each select="books/book">
					<!-- <w:p w14:paraId="1EF25F73" w14:textId="28953BE3" w:rsidR="000B2851" w:rsidRDefault="000B2851" w:rsidP="000B2851">
						<w:pPr>
							<w:pStyle w:val="Heading2" />
						</w:pPr>
						<w:r>
							<w:t>
								<xsl:value-of select="@title" />
							</w:t>
						</w:r>
						<w:bookmarkStart w:id="0" w:name="_GoBack" />
						<w:bookmarkEnd w:id="0" />
					</w:p> -->
					<!-- for-each loop added for Movie.  This loop includes the Open XML elements that define the   paragraph as a bulleted list. -->
					<!-- <xsl:for-each select="book"> -->
						<w:p>
							<w:pPr>
								<w:pStyle w:val="ListParagraph" />
								<w:numPr>
									<w:ilvl w:val="0" />
									<w:numId w:val="1" />
								</w:numPr>
							</w:pPr>
							<w:r>
								<w:t>
									<!-- Movie Title placeholder replaced by the Movie's Name element in the XML data file. -->
									<xsl:value-of select="title"/>
								</w:t>
							</w:r>
							<w:r>
								<w:t>
									
								</w:t>
							</w:r>
							<w:r w:rsidRPr="000B2851">
								<!-- Year placeholder replaced by the Movie's Released element in the XML data file. -->
								<w:t>
								(<xsl:value-of select="publisher/year"/>)
								</w:t>
							</w:r>
						</w:p>
						<w:sectPr w:rsidR="000B2851">
							<w:pgSz w:w="12240" w:h="15840" />
							<w:pgMar w:top="1440" w:right="1440" w:bottom="1440" w:left="1440" w:header="720" w:footer="720" w:gutter="0" />
							<w:cols w:space="720" />
							<w:docGrid w:linePitch="360" />
						</w:sectPr>
					<!-- </xsl:for-each> -->
				</xsl:for-each>
			</w:body>
		</w:document>
	</xsl:template>
</xsl:stylesheet>