const fs = require("fs");
const convert = require("xml-js");
var Ajv = require("ajv");
var ajv = new Ajv();

const fetchXml = section => {
  // This should be a call to a service, as the schema will have to be
  // dynamic in parts, e.g.:
  //   "categoryId": {
  //     "type": "integer",
  //     "title": "Dossier Category",
  //     "enum": [1, 2, 3],
  //     "enumNames": ["one", "two", "three"]
  //   }
  // enum and enumName will have to read from the database.

  const bookJsonSchema = JSON.parse(
    fs.readFileSync("./service/BooksSchema.json", "utf8")
  );

  const bookJson = JSON.parse(
    fs.readFileSync("./service/BooksDataSample.json", "utf8")
  );

  var validate = ajv.compile(bookJsonSchema);
  var valid = validate(bookJson);
  if (!valid) {
    console.log("ERRORS!");
    console.log(validate.errors);
  }

  const transform = bookJson.books.map(item => {
    return {
      book: {
        _attributes: {
          id: item.id
        },
        ...item
      }
    };
  });

  const result = convert.json2xml(
    JSON.stringify({
      _declaration: {
        _attributes: {
          version: "1.0",
          encoding: "utf-8"
        }
      },
      books: transform
    }),
    {
      compact: true,
      spaces: 2
    }
  );

  return result;
};

module.exports.fetchXml = fetchXml;
