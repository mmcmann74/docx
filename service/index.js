var path = require("path");
var fs = require("fs");
const express = require("express");
const { createDocx } = require("./createDocx");

const app = express();
const port = 3000;

app.get("*/*", async (req, res, next) => {
  const buffer = await createDocx();
  res.setHeader("Content-disposition", "attachment; filename=dossier.docx");
  res.setHeader(
    "Content-type",
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
  );
  res.end(Buffer.from(buffer, "base64"));
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
